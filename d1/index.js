/*
	Functions are lines/blocks of codes that tell our devices/application to perform certain task/s when called/invoked.
	Functions are mostly created to create complicated tasks to run several lines of codes in succession.
	They are also used to prevent repeatition of typing lines/blocks of codes that perform the same task.
*/
/*
	Miniactivity
		create a function
			create a let variable nickname, the value should come from a prompt();
			log in the console the variable

		invoke the function
*/

/*function printInput(){
	let nickname = prompt( "Enter your nickname" );
	console.log( nickname );
};

printInput();*/


// However, for some use cases, this may not be ideal. For other cases, functions can also process data directly passed into it instead of relying only on global variables such as prompt();

// Consider this function:
// we can directly pass data into thhe function. as for the function below, the data is referred to as the "name" within the printName function. this "name" is what we called as the parameter.
/*
	PARAMETER
		-it acts as a named variable/container that exists only inside of a function;
		-it is used to store information that is provided to a function when it is called/invoked;
*/
function printName(name){
	console.log( "Hello " + name );
};
// ARGUMENTS
// "Joana", the information provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.
printName( "Joana" );
// These two are both arguments since both of them are supplied as information that will be used to print out the full message of function. when the printName() function is called, "John" and "Jane" are stored in the paramater "name" then the function uses them to  print the message
printName("John");
printName("Jane");

let sampleVar = "Yua";
printName(sampleVar);

/*
	Miniactivity
		create 2 functions
			1st function should have 2 parameters (separated with a comma)
				log in the console the message "the numbers passed as arguments are:"
				log in the console the first parameter (number) 
				log in the console the second parameter (number) 
			invoke the function
			
			2nd function should have 3 parameters (make use of comma to separate the parameters)
				log in the console "My friends are parameter1, parameter2, parameter3"
			invoke the function

			send the output in the batch google chat
*/
// multiple parameters are separated by comma
function printNumbers(num1, num2){
	console.log( "The numbers passed as arguments are: " );
	console.log( num1 );
	console.log( num2 );
}
// argument that will be stored in multiple parameters should also be separated by comma
printNumbers(5, 2);


function printFriends(friend1, friend2, friend3){
	console.log( "My friends are " + friend1 + " " + friend2 + " " + friend3 + "." );
}
printFriends( "Martin", "Kevin", "Jerome" );

// improving our codes for the last activity with the use of parameters
function checkDivisibilityBy8(num){
	let remainder = num%8;
	console.log("The remainder of " + num + " is " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);
// We can also do the same using prompt(). However, take note that using prompt() outputs a string data type. Strings data types are not ideal and cannot be used for mathematical computations
/*
	Miniactivity
	improve the activity for the isDivisibleBy4 function in the last activity by using paramters instead of prompt()
	send the output in the google chat
*/

function checkDivisibilityBy4(num){
	let remainder = num % 4;
	console.log("The remainder of " + num + " is " + remainder);
	let isDivisibleBy4 = remainder === 0;
	console.log( "Is " + num + " divisible by 4?" );
	console.log( isDivisibleBy4 );
}

checkDivisibilityBy4(84);
checkDivisibilityBy4(25);

// FUNCTION DEBUGGING PRACTICE (no need to send the screenshot :))
function isEven(num){
	console.log(num % 2 === 0);
};

function isOdd(num1){
	console.log (num1 % 2 !== 0);
}

isEven(20);
isEven(21);
isOdd(31);
isOdd(30);

/*
FUNCTIONS AS ARGUMENTS
	function parameters can also accept other functions as arguments
	some complex functions use other functions as arguments to perform complicated tasks/result
		This will be further discussed when we tackle array methods
*/
function argumentFunction(){
	console.log("This function is passed into another function.");
};

function invokeFunction(functionParameter){
	console.log("This is from invokeFunction.")
	functionParameter();
}
// adding and removing of the parenthesis impacts the output of JS heavily. when a function is used with parenthesis, it denotes that invoking/calling a function - that is why when we call the function, the functionParameter as an argument does not have any parenthesis. 
// if the function as parameter does not have parenthesis, the function as an argument should have parenthesis to denote that it is a function being passed as an argument
invokeFunction(argumentFunction);

/*function printFullName( firstName, middleName, lastName ){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}
printFullName('Juan', 'Dela', 'Cruz');*/
// Trying to achieve the same the result as the function above
/*let firstName = "Juan";
let middleName = "Dela";
let lastName = "Cruz";
console.log(firstName + " " + middleName + " " + lastName);*/

// Return Statement
// return statement allows us to output a value from a function to passed to the line/block of code that invoked/called our function; without it, there will be no defined value assigned to the function, once it is called
function returnFullName( firstName, middleName, lastName ){
	// console.log("this is printed"); - this is printed because it is coded before the line where the return statement is located
	return firstName + " " + middleName + " " + lastName;
	// console.log( "This message will not be printed" ); - this line is not printed/executed because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution
}
/*
this creates a value based on the return statement in the returnFullName function
returnFullName("Jeffrey", "Smith", "Bezos")
*/
// whatever value is returned, it will be the value for that function with use of the arguments passed when the function is called.
console.log(returnFullName("Jeffrey", "Smith", "Bezos"));

//the value returned from a function can also be stored inside a variable
let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);

/*
	make use of 2 parameters to show the full address of a person
		through use of return statement, create a value for the function once it is called passing 2 arguments inside the function
	log in the console the returned value for the function

		send the output in the google chat
*/
function returnAddress(city, country){
	let fullAddress = city + ", " +  country;
	return fullAddress;
};

let myAddress = returnAddress("Batangas City", "Philippines") ;
console.log(myAddress);


function printPlayerInfo(username, level, job){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
}

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
// returns undefined because printPlayerInfo function returns nothing. its task is only to print the messages in the console, but not to return any value. thus, we cannot save any value from printPlayerInfo because it does not return anything.
console.log(user1);